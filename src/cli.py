# -*- coding: utf-8 -*-

import sys

from textwrap import dedent
from docopt import docopt
from termcolor import cprint


__version__ = '0.1.0'
__doc__ = """T-CLI - The Command Line Interface.

Usage:
  cli.py android
  cli.py mine (set|remove) <x> <y> [--moored | --drifting]
  cli.py option <key>...
  cli.py ship <name> move <x> <y> [--speed=<kn>]
  cli.py ship shoot <x> <y>
  cli.py (-h | --help)
  cli.py --version

Options:
  -h --help     Show this screen.
  --version     Show version.
  --speed=<kn>  Speed in knots [default: 10].
  --moored      Moored (anchored) mine.
  --drifting    Drifting mine.

"""


def print_version():
    print(f'version: {__version__}')


def cli(options):
    exit_status = 0

    if options.get('--version'):
        print_version()
    elif options.get('option'):
        if options.get('<key>'):
            keys = options.get('<key>')

            # specific_opt = {k: options[k] for k in keys}
            # cprint(f'{specific_opt}', 'green')

            for k, v in options.items():
                if k in keys:
                    cprint(f'{k, v}', 'green')
        else:
            cprint(options, 'green')
    elif options.get('android'):
        print(android())
    else:
        return None

    return exit_status


def main():
    options = docopt(__doc__)
    # print(options)
    exit_status = cli(options=options)

    if exit_status is None:
        print('Not implemented.')
        print(options)
        exit_status = 2

    sys.exit(exit_status)


def android():
    return dedent(
        '''
        ┈┈┈╲┈┈┈┈╱
        ┈┈┈╱▔▔▔▔╲
        ┈┈┃┈▇┈┈▇┈┃
        ╭╮┣━━━━━━┫╭╮
        ┃┃┃┈┈┈┈┈┈┃┃┃
        ╰╯┃┈┈┈┈┈┈┃╰╯
        ┈┈╰┓┏━━┓┏╯
        ┈┈┈╰╯┈┈╰╯''')


if __name__ == '__main__':
    main()
