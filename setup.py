# -*- coding: utf-8 -*-
import os
from io import open
from setuptools import find_packages, setup

package_name = 'test.cli'

# A tagged build means it's a release. Use GIT tag to form the version number.
# https://docs.gitlab.com/ee/ci/variables/#predefined-environment-variables
__version__ = '0.1.0'

requires = [
    'docopt',
    'envparse',
    'gitpython',
    'pyaml',
    'semver',
    'termcolor',
]

test_requires = [
    'Sphinx',
    'faker',
    'flake8',
    'freezegun',
    'future',
    'pytest-cov',
    'pytest-faker',
    'pytest-helpers-namespace',
    'pytest-mock',
    'sphinx_rtd_theme',
    'twine',
    'virtualenv',
    'wheel',
]

with open('README.md', mode='r', encoding='utf-8') as f:
    readme = f.read()

setup(
    author='Partiban Ramasmay',
    name=package_name,
    version=__version__,
    description='The Command Line Interface.',
    long_description=readme,
    install_requires=requires,
    tests_require=test_requires,
    extras_require={'test': test_requires},
)
